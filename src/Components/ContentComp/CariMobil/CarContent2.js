import React, { useState, useEffect } from "react";
import CarContent3 from "./CarContent3";
import CarFilter from "./CarFilter";

export const CarContent2 = () => {
  const [type, setType] = useState("Pilih Tipe Driver");
  const [date, setDate] = useState("Pilih Waktu");
  const [pickupTime, setPickupTime] = useState("");
  const [passenger, setPassenger] = useState("");
  const [cars, setCars] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (type !== "Pilih Tipe Driver") {
      const pass = passenger ? passenger : "0";
      const filter = { type, date, pickupTime, pass };
      fetch("http://localhost:5000/api/cars", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(filter),
      })
        .then((res) => res.json())
        .then((cars) => setCars(cars));
    }
  };

  const handleType = (event) => {
    setType(event.target.value);
  };
  const handleDate = (event) => {
    setDate(event.target.value);
  };
  const handlePickupTime = (event) => {
    setPickupTime(event.target.value);
  };
  const handlePassenger = (event) => {
    setPassenger(event.target.value);
  };

  useEffect(() => {
    console.log(cars);
  }, [cars]);

  return (
    <>
      <div className="container filter justify-content-center border-3 rounded-3 shadow-lg p-3 mb-1 bg-body rounded z-index-3 position-relative">
        <div>
          <form onSubmit={handleSubmit}>
            <div className="row align-items-center justify-content-center">
              <div className="col-lg-3 my-1">
                <label htmlFor="typeDriver">Tipe Driver</label>
                <br />
                <select
                  value={type}
                  required
                  onChange={handleType}
                  className="form-select type"
                  name="typeDriver"
                  id="typeDriver"
                >
                  <option disabled hidden>
                    Pilih Tipe Driver
                  </option>
                  <option value="Dengan Sopir">Dengan Sopir</option>
                  <option value="Keyless Entry">
                    Tanpa Sopir (Lepas Kunci)
                  </option>
                </select>
              </div>
              <div className="col-lg-3 my-1">
                <label htmlFor="date">Pilih Tanggal</label>
                <br />
                <input
                  className="form-control"
                  onChange={handleDate}
                  required
                  type="date"
                  name="date"
                  id="date"
                />
              </div>
              <div className="col-lg-2 my-1">
                <label htmlFor="time">Waktu Ambil</label>
                <br />
                <select
                  value={pickupTime}
                  onChange={handlePickupTime}
                  className="form-select time"
                  name="time"
                  id="time"
                >
                  <option value="8">08.00 WIB</option>
                  <option value="9">09.00 WIB</option>
                  <option value="10">10.00 WIB</option>
                  <option value="11">11.00 WIB</option>
                  <option value="12">12.00 WIB</option>
                </select>
              </div>
              <div className="col-lg-2 my-1">
                <label htmlFor="passenger">Penumpang</label>
                <br />
                <select
                  value={passenger}
                  onChange={handlePassenger}
                  className="form-select"
                  name="passenger"
                  id="passenger"
                >
                  <option selected>Jumlah Penumpang</option>
                  <option value="2"> 2 atau lebih </option>
                  <option value="4"> 4 atau lebih</option>
                  <option value="6"> 6 atau lebih</option>
                </select>
              </div>
              <div className="col-lg-2 my-1 d-flex flex-column justify-content-center">
                <p></p>
                <button className="btn btn-success submit">Cari Mobil</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      {cars ? <CarFilter cars={cars} /> : <CarContent3 />}
    </>
  );
};
